#!/usr/bin/env bash
venv=ppt-venv
python3.10 -m venv ${venv}
. ${venv}/bin/activate
pip3 install -U pip
pip3 install -U wheel
pip3 install -U -e .
pip3 install -r dev-requirements.txt
cat <<EOF > activate.sh
#!/usr/bin/env bash
PPT_HOME=${PWD}
. \${PPT_HOME}/${venv}/bin/activate
hash -r
EOF
chmod +x activate.sh
