import re
from pathlib import Path

from setuptools import setup, find_packages

HERE = Path(__file__).resolve().parent

version_re = re.compile(r"^__version__\s*=\s*'(?P<version>.*)'$", re.M)
def version():
    match = version_re.search(Path('ppt/__init__.py').read_text())
    if match:
        return match.groupdict()['version'].strip()
    raise AttributeError(
        'Could not find __version__ attribute in ppt/__init__.py'
    )

long_description = Path(HERE, 'README.md').resolve().read_text()

setup(
    name='pptoolz',
    packages=find_packages(
        exclude=['tests', 'powershell'],
    ),
    package_dir={
        'ppt': 'ppt',
    },
    package_data = {
        'ppt': [
            'data/*.txt',
            'data/*.csv',
            'data/*.gz',
        ],
    },

    install_requires=[
        'bs4',
        'chardet',
        'click',
        'coloredlogs',
        'ifcfg',
        'impacket',
        'jinja2',
        'jmespath',
        'lxml',
        'markdown',
        'msgpack',
        'multipledispatch',
        'networkx',
        'paramiko',
        'pillow',
        'pycryptodome',
        'pymaybe',
        'pymdown-extensions',
        'pyperclip',
        'pyrsistent',
        'python-dateutil',
        'regex',
        'requests',
        'requests[socks]',
        'ruamel.yaml',
        'toolz',
        'webdav',
        'xmljson',
        'openpyxl',
    ],

    version=version(),
    description=(
        'Collection of utilities'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',

    url='https://gitlab.com/primitivepogue/pptoolz.git',

    author="Pogue Mahone",
    author_email='primitivepogue@protonmail.com',

    license='MIT',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.10',
    ],

    zip_safe=False,

    keywords=('stuff'),

    scripts=[
    ],

    entry_points={
        'console_scripts': [
        ],
    },
)
